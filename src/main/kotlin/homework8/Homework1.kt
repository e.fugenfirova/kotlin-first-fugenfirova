package homework8

fun main() {
    val sourceList = mutableListOf(1, 2, 3, 1, 2, 3, 1, 1, 5, 6, 8, 1, 8, 7, 4, 9)
    println(makeUnique(sourceList))
    println(makeUnique2(sourceList))

}

fun makeUnique(list: List<Int>): List<Int> {
    val exitList = mutableListOf<Int>()
    for (item in list) {
        if (item !in exitList) exitList.add(item)
    }
    return exitList
}

fun makeUnique2(list: List<Int>): List<Int> {
    return list.toSet().toMutableList()
}