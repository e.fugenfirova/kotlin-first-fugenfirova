package homework8

val userCart = mutableMapOf<String, Int>("potato" to 2, "cereal" to 2, "milk" to 1, "sugar" to 3, "onion" to 1, "tomato" to 2, "cucumber" to 2, "bread" to 3)
val discountSet = setOf("milk", "bread", "sugar")
val discountValue = 0.20
val vegetableSet = setOf("potato", "tomato", "onion", "cucumber")
val prices = mutableMapOf<String, Double>(
    "potato" to 33.0,
    "sugar" to 67.5,
    "milk" to 58.7,
    "cereal" to 78.4,
    "onion" to 23.76,
    "tomato" to 88.0,
    "cucumber" to 68.4,
    "bread" to 22.0
)

fun main() {
    println(calcSum(userCart))
    println(calcVeg(userCart))
}

fun calcSum(cart: Map<String, Int>): Double {
    var sum = 0.0
    for ((name, quantity) in cart) {
        val price = prices[name] ?: continue
        sum += if (name in discountSet) {
            (price - (price * discountValue)) * quantity
        } else {
            price * quantity
        }
    }
    return sum
}

fun calcVeg(cart: Map<String, Int>): Int {
    var vegs = 0
    for ((name, quantity) in cart) {
        if (name in vegetableSet) vegs += quantity
    }
    return vegs
}
