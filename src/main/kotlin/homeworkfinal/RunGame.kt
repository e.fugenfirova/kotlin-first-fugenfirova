package homeworkfinal

fun main() {
    print("Введите имя первого игрока: ")
    val playerA = Player(name = readLine()!!)
    print("Поместите корабли первого игрока. ")
    playerA.placeShips()
    println("Позиции кораблей игрока ${playerA.name}: ${playerA.position}")

    print("Введите имя второго игрока: ")
    val playerB = Player(name = readLine()!!)
    print("Поместите корабли второго игрока. ")
    playerB.placeShips()
    println("Позиции кораблей игрока ${playerB.name}: ${playerB.position}")

    println("Игра началась!")
    while (playerB.position.isNotEmpty() && playerA.position.isNotEmpty()) {
        println("Ход игрока ${playerA.name}: ")
        playerA.hit(playerB.position)
        if (playerB.position.isNotEmpty()) {
            println("Ход игрока ${playerB.name}: ")
            playerB.hit(playerA.position)
        }
    }

    println("Игра закончена")
    when {
        playerB.position.isEmpty() -> println("Игрок ${playerA.name} выиграл")
        playerA.position.isEmpty() -> println("Игрок ${playerB.name} выиграл")
    }
}
