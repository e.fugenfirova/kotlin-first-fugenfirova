package homeworkfinal

class Player(val name: String) {
    var position = mutableMapOf<String, MutableList<String>>()
    private val positionNeighbours = mutableListOf<String>()

    fun placeShips() {
        var shipNumber = 1
        val fleet = shipSettings
        for (ship in fleet) {
            print("Введите ячейку для корабля $shipNumber (палуб: ${ship.decks}): ")
            val selectedDecks = ship.selectPosition(position, positionNeighbours)
            for (deck in selectedDecks) {
                positionNeighbours.addAll(createNeighbourCells(deck.toList()))
            }
            position["Корабль $shipNumber"] = selectedDecks.toMutableList()
            ++shipNumber
        }
    }

    fun hit(opponentPosition: MutableMap<String, MutableList<String>>) {
        while (opponentPosition.isNotEmpty()) {
            val field = readLine()!!
            val foundShip = opponentPosition.entries.firstOrNull { (_, shipDecks) -> field in shipDecks }

            when {
                foundShip == null -> {
                    println("Неверный ход")
                    break
                }

                foundShip.value.size == 1 -> {
                    println("Убил!")
                    opponentPosition.remove(foundShip.key)
                }

                else -> {
                    println("Ранил!")
                    foundShip.value.remove(field)
                }
            }
        }
    }

    private fun createNeighbourCells(cell: List<Char>): List<String> {
        val neighbourCells = mutableListOf<String>()
        for (x in cell[0] - 1..cell[0] + 1) {
            for (y in cell[1] - 1..cell[1] + 1) {
                if (x == cell[0] && y == cell[1]) continue
                neighbourCells.add("$x$y")
            }
        }

        return neighbourCells
    }
}
