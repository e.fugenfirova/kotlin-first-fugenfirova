package homeworkfinal

import kotlin.math.abs

class Ship(val decks: Int) {
    private val formattingMessage = "Ячейка должна быть в формате $coordinatesX / $coordinatesY. Например - C3. " +
        "Попробуйте еще раз."
    private val conflictMessage = "Ячейка занята, выберите другую."

    private fun checkTwoDecksCellsAreNear(neighbourCell: String, verifiedCell: String): Boolean {
        return if (neighbourCell.length == 2 && verifiedCell.length == 2) {
            if (
                (neighbourCell[0] == verifiedCell[0] && abs(verifiedCell[1].code - neighbourCell[1].code) == 1) ||
                (neighbourCell[1] == verifiedCell[1] && abs(verifiedCell[0].code - neighbourCell[0].code) == 1)
            ) {
                true
            } else {
                println("Ячейки многопалубного корабля должны находиться рядом. Попробуйте еще раз.")
                false
            }
        } else {
            false
        }
    }

    fun checkCell(
        cell: String,
        position: Map<String, List<String>>,
        positionNeighbours: List<String>
    ): Boolean {
        return when {
            cell.length != 2 || cell[0] !in coordinatesX || cell[1].digitToIntOrNull() !in coordinatesY -> {
                println(formattingMessage)
                false
            }

            cell in position.values.flatten() -> {
                println(conflictMessage)
                false
            }

            positionNeighbours.contains(cell) -> {
                println("Расстояние от другого корабля - минимум 1 ячейка.")
                false
            }

            else -> true
        }
    }

    fun selectPosition(
        playerShips: Map<String, List<String>>,
        neighbourCells: List<String>
    ): List<String> {
        val severalFields: MutableList<String> = mutableListOf()
        var field = readLine()!!
        while (!checkCell(field, playerShips, neighbourCells)) {
            field = readLine()!!
        }
        severalFields += field

        while (severalFields.size < decks) {
            print("Введите следующую ячейку для корабля (палуб: $decks): ")
            var nextField = (readLine()!!)
            while (
                !checkCell(nextField, playerShips, neighbourCells) ||
                !checkTwoDecksCellsAreNear((severalFields[severalFields.size - 1]), nextField)
            ) {
                nextField = (readLine()!!)
            }
            severalFields += nextField
        }

        return severalFields
    }
}
