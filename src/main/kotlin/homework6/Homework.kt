package homework6

abstract class Animal (open val name: String, open val height: Int, open val weight: Int) {
    abstract var satiety: Int /* Не стала инициализировать ее здесь как 0, хотя если на старте у всех 0, это имело бы смысл.
    Предположим, что насыщение может быть не нулевым и объявляться у конкретного вида животного
    */
    abstract var preferences: Array<String>
    fun eat(food: Array<String>) {
        for (piece in food) {
            if (piece in preferences) ++satiety
        }
    } // Сделала неабстрактной, обсуждали в тайме
}

abstract class Predator(name: String, height: Int, weight: Int) : Animal(name, height, weight) {
    override var preferences: Array<String> = arrayOf("Антилопа", "Заяц", "Косуля")
}

abstract class Herbivore(name: String, height: Int, weight: Int) : Animal(name, height, weight) {
    override var preferences: Array<String> = arrayOf("Трава", "Листочки", "Овес")
}

abstract class Primates(name: String, height: Int, weight: Int) : Animal(name, height, weight) {
    override var preferences: Array<String> = arrayOf("Насекомые", "Фрукты", "Орехи")
}

class Lion(
    override val name: String, /* Также непонятно, зачем делать именно через оверрайд,
    ведь мы все равно даем значение конкретному животному. Оставила как в задании */
    override val height: Int,
    override val weight: Int,
    override var satiety: Int = 0
) : Predator(name, height, weight)

class Tiger (
    override val name: String,
    override val height: Int,
    override val weight: Int,
    override var satiety: Int = 0
) : Predator(name, height, weight)

class Hippo (
    override val name: String,
    override val height: Int,
    override val weight: Int,
    override var satiety: Int = 0
) : Herbivore(name, height, weight)

class Wolf (
    override val name: String,
    override val height: Int,
    override val weight: Int,
    override var satiety: Int = 0
) : Predator(name, height, weight)

class Giraffe (
    override val name: String,
    override val height: Int,
    override val weight: Int,
    override var satiety: Int = 0
) : Herbivore(name, height, weight)

class Elephant (
    override val name: String,
    override val height: Int,
    override val weight: Int,
    override var satiety: Int = 0
) : Herbivore(name, height, weight)

class Chimpanzee (
    override val name: String,
    override val height: Int,
    override val weight: Int,
    override var satiety: Int = 0
) : Primates(name, height, weight)

class Gorilla (
    override val name: String,
    override val height: Int,
    override val weight: Int,
    override var satiety: Int = 0
) : Primates(name, height, weight)


fun Unit(animals: Array<Animal>, food: Array<String>) {
    for (animal in animals) {
        animal.eat(food)
        println("Насыщение животного ${animal.name}: ${animal.satiety}")
    }
}


fun main() {
    val lion = Lion("Лев", 160, 100)
    val tiger = Tiger("Тигр", 150, 100)
    val hippo = Hippo("Бегемот", 150, 500)
    val wolf = Wolf("Волк", 80, 80)
    val giraffe = Giraffe("Жираф", 350, 100)
    val elephant = Elephant("Слон", 150, 100)
    val chimpanzee = Chimpanzee("Шимпанзе", 150, 100)
    val gorilla = Gorilla("Горилла", 150, 100)

    val zoo = arrayOf(lion, tiger, hippo, wolf, giraffe, elephant, chimpanzee, gorilla)
    val food = arrayOf("Антилопа", "Косуля", "Трава", "Овес", "Листочки", "Орехи")
    Unit(zoo, food)
}
