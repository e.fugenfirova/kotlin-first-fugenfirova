package homework4

fun main() {
    val myArray = arrayOf(1, 25, 10, 6, 22, 17, 8, 14, 23)
    var captainIncome = 0
    var crewIncome = 0
    for(i in myArray.indices) {
        if (myArray[i] > 0) {
            if (myArray[i] % 2 == 0) {
                ++captainIncome
            } else {
                ++crewIncome
            }
        }
    }
    print("Капитан получит монет: $captainIncome, команда получит монет: $crewIncome")
}

