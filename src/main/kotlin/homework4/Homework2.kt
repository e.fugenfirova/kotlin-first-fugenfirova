package homework4

fun main() {
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)
    var excellent = 0
    var good = 0
    var sufficient = 0
    var bad = 0
    for (i in marks.indices) {
        when (marks[i]) {
            2 -> ++bad
            3 -> ++sufficient
            4 -> ++good
            5 -> ++excellent
        }
    }

    fun calc(arg: Int): Double {
        return (arg.toDouble() / marks.size * 100)
    }

    print("Отличников: ${(calc(excellent))}%, хорошистов: ${calc(good)}%," +
            "троечников: ${calc(sufficient)}%, двоечников: ${calc(bad)}%")
}
