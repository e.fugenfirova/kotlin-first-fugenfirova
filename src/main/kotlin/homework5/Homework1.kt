package homework5

open class Animal (val name: String, val height: Int, val weight: Int) {
    var satiety = 0
    private lateinit var preferences: Array<String>
    constructor(name: String, height: Int, weight: Int, preferences: Array<String>, satiety: Int = 0)
            : this(name, height, weight) {
        this.preferences = preferences
        this.satiety = satiety
    }
    fun eat(food: String) {
        if(food in preferences) ++satiety
    }
}

class Lion(
    name: String,
    height: Int,
    weight: Int,
    preferences: Array<String>) : Animal(name, height, weight, preferences)

class Tiger (
    name: String,
    height: Int,
    weight: Int,
    preferences: Array<String>) : Animal(name, height, weight, preferences)

class Hippo (
    name: String,
    height: Int,
    weight: Int,
    preferences: Array<String>) : Animal(name, height, weight, preferences)

class Wolf (
    name: String,
    height: Int,
    weight: Int,
    preferences: Array<String>) : Animal(name, height, weight, preferences)

class Giraffe (
    name: String,
    height: Int,
    weight: Int,
    preferences: Array<String>) : Animal(name, height, weight, preferences)

class Elephant (
    name: String,
    height: Int,
    weight: Int,
    preferences: Array<String>) : Animal(name, height, weight, preferences)

class Chimpanzee (
    name: String,
    height: Int,
    weight: Int,
    preferences: Array<String>) : Animal(name, height, weight, preferences)

class Gorilla (
    name: String,
    height: Int,
    weight: Int,
    preferences: Array<String>) : Animal(name, height, weight, preferences)

fun main() {
    val lion = Lion("Лев", 160, 100, arrayOf("Антилопа", "Заяц", "Косуля"))
    val tiger = Tiger("Тигр", 150, 100, arrayOf("Антилопа", "Заяц", "Косуля"))
    val hippo = Hippo("Бегемот", 150, 500, arrayOf("Трава"))
    val wolf = Wolf("Волк", 80, 80, arrayOf("Косуля", "Заяц", "Трава"))
    val giraffe = Giraffe("Жираф", 350, 100, arrayOf("Трава", "Листочки"))
    val elephant = Elephant("Слон", 150, 100, arrayOf("Трава", "Сено"))
    val chimpanzee = Chimpanzee("Шимпанзе", 150, 100, arrayOf("Насекомые", "Фрукты", "Вода"))
    val gorilla = Gorilla("Горилла", 150, 100, arrayOf("Орехи", "Бамбук", "Вода"))

    val zoo = arrayOf(lion, tiger, hippo, wolf, giraffe, elephant, chimpanzee, gorilla)
    for (animal in zoo) {
        animal.eat("Антилопа")
        animal.eat("Косуля")
        animal.eat("Трава")
    }
    for (animal in zoo) {
        println("Насыщение животного ${animal.name}: ${animal.satiety}")
    }
}
