package homework5

fun main() {
    val digit = readLine()!!.toInt()
    print(calc(digit))
}

fun calc(number: Int): Int{
    var newNumber = 0
    var n = number
    while(n > 0) {
        val remainder = n % 10
        newNumber *= 10
        newNumber += remainder
        n /= 10
    }
    return newNumber
}
