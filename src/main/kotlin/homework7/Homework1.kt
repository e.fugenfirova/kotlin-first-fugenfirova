package homework7

fun main() {
    print("Введите логин: ")
    val login = readLine()!!
    print("Введите пароль: ")
    val password = readLine()!!
    print("Подтвердите пароль: ")
    val confirmPassword = readLine()!!
    print("Юзер ${signUp(login, password, confirmPassword).login} успешно создан")
}

class WrongLoginException(message: String) : Exception(message)
class WrongPasswordException(message: String): Exception(message)
class User(val login: String, private val password: String)
fun signUp(login: String, password: String, confirmPassword: String): User {
    when {
        login.length > 20 -> throw WrongLoginException("Логин не может быть длиннее 20 символов")
        password.length < 10 -> throw WrongPasswordException("Пароль не может быть короче 10 символов")
        confirmPassword != password -> throw WrongPasswordException("Пароли не совпадают")
    }
    return User(login, password)
}

