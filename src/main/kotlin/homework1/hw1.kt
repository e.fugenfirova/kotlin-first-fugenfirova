package homework1

// Не стала убирать redundant обозначения типов, т.к. решила, что для первого дз стоит явно их обозначить.
// По идее можно избавиться от int, float, double и string обозначений

fun main() {
    val lemonade: Int = 18500
    val pinaCollada: Short = 200
    val whiskey: Byte = 50
    val freshDrops: Long = 3000000000
    val cola: Float = 0.5F
    val ale: Double = 0.666666667
    val improvisation: String = "Что-то авторское!"

    println("Заказ '$lemonade мл лимонада' готов!")
    println("Заказ '$pinaCollada мл пина колады' готов!")
    println("Заказ '$whiskey мл виски' готов!")
    println("Заказ '$freshDrops капель фреша' готов!")
    println("Заказ '$cola литра колы' готов!")
    println("Заказ '$ale литра эля' готов!")
    println("Заказ '$improvisation' готов!")
}
